//
//  ViewController.swift
//  TestAppPictureGallery
//
//  Created by Артур on 20.08.2021.
//

import UIKit
import MessageUI

class ViewController: UIViewController {
    
    @IBOutlet private weak var collectionImagesView: CollectionView!
    @IBOutlet private weak var fullPictireView: UIView!
    @IBOutlet private weak var fullImageView: UIImageView!
    @IBOutlet private weak var searchBar: UISearchBar!
    
    private let projectManager = ProjectManager.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.projectManager.delegate = self
        
        self.collectionImagesView.dataSource = self
        self.collectionImagesView.delegate = self
        self.searchBar.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @IBAction func closeFullPirtureView(_ sender: Any) {
        self.fullPictireView.isHidden = true
    }
    
    @IBAction func postEmail(_ sender: Any) {
        if let image = self.fullImageView.image {
            guard let mailController = projectManager.postEmail(image) else { return }
            self.present(mailController, animated: true, completion:nil)
        }
    }
}

extension ViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        let searchActive = !projectManager.indexesOfElementsFoundArray.isEmpty
        let notResultOfSearch = !projectManager.notResultOfSearch
        
        if notResultOfSearch {
            return 0
        }
        return searchActive ? projectManager.indexesOfElementsFoundArray.count : projectManager.dataOfImageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell",
                                                      for: indexPath) as! CollectionViewCell
        let defaultIndex = indexPath.row
        let searchActive = !projectManager.indexesOfElementsFoundArray.isEmpty
            
        cell.titleLabel.text = searchActive ?
            projectManager.dataOfImageArray[projectManager.indexesOfElementsFoundArray[defaultIndex]].titleOfImage:
            projectManager.dataOfImageArray[defaultIndex].titleOfImage
            
        cell.photoImageView.image = searchActive ?
            projectManager.dataOfImageArray[projectManager.indexesOfElementsFoundArray[defaultIndex]].miniatureImage:
            projectManager.dataOfImageArray[defaultIndex].miniatureImage
            
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let defaultIndex = indexPath.row
        let searchActive = !projectManager.indexesOfElementsFoundArray.isEmpty
        self.fullPictireView.isHidden = false
        
        DispatchQueue.main.async {
            if searchActive {
                let mutableIndex = self.projectManager.indexesOfElementsFoundArray[defaultIndex]
                self.fullImageView.image = self.projectManager.getImage(link: self.projectManager.dataOfImageArray[mutableIndex].fullImage!)
            } else {
                self.fullImageView.image = self.projectManager.getImage(link: self.projectManager.dataOfImageArray[defaultIndex].fullImage!)
            }
        }
    }
}

extension ViewController: UISearchControllerDelegate, UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        self.projectManager.getSearchingIndexes(searchText: searchText)
        self.collectionImagesView.reloadData()
    }
}

extension ViewController: ProjectDataProtocol {
    
    func updateData() {
        DispatchQueue.main.async {
            self.collectionImagesView.reloadData()
        }
    }
    
    
}
