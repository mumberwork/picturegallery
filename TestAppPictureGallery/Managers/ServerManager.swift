//
//  ServerManager.swift
//  TestAppPictureGallery
//
//  Created by Артур on 20.08.2021.
//

import UIKit

protocol ServerDataProtocol: AnyObject {
    func setData(_ arrayOfDataImages: [DataOfImage])
}

class ServerManager: NSObject {

    weak var delegate: ServerDataProtocol?
    var dataOfImage = [DataOfImage]()
    
    func requetsData() {
        
        let url = URL(string:"https://www.xiag.ch/share/testtask/list.json")
        
            if let url = url {
                URLSession.shared.dataTask(with: url) { (data, response, error) in
                    
                    if let error = error  {
                        print(error.localizedDescription)
                    }
                    if let data = data {
                        if let jsonData = try? JSONSerialization.jsonObject(with: data, options: []) as? NSArray {
                            self.parsData(dataArrayOfImages: jsonData)
                            
                            
                        }
                    }
                }.resume()
            }
        }
    
    func parsData(dataArrayOfImages: NSArray) {
        
        for i in 0 ..< dataArrayOfImages.count {
            if let dataDictImage = dataArrayOfImages[i] as? NSDictionary,
               var linkMiniatureImage = dataDictImage["url_tn"] as? String,
               var linkFullImage = dataDictImage["url"] as? String,
               let titleOfImage = dataDictImage["name"] as? String {
                
                linkFullImage.insert(string: "s", ind: 4)
                linkMiniatureImage.insert(string: "s", ind: 4)
                
                let miniatureImage = self.getImageFromUrl(link: linkMiniatureImage)
                
                dataOfImage.append(DataOfImage(miniatureImage: miniatureImage, fullImage: linkFullImage, titleOfImage: titleOfImage))
            }
            delegate?.setData(self.dataOfImage)
        }
    }
    
    func getImageFromUrl(link: String) -> UIImage {
        
        let url = URL(string: link)
        var image = UIImage()
        
        let data = try? Data(contentsOf: url!)
        if let data = data {
            image = UIImage(data: data)!
        }
        return image
    }
}

