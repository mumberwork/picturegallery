//
//  ProjectManager.swift
//  TestAppPictureGallery
//
//  Created by Артур on 20.08.2021.
//

import UIKit
import MessageUI

class ProjectManager: NSObject {
    
    private let server = ServerManager()
    static let shared = ProjectManager()
    
    weak var delegate: ProjectDataProtocol?
    
    var dataOfImageArray = [DataOfImage]()
    var indexesOfElementsFoundArray = [Int]()
    var notResultOfSearch = true
    
    required override init() {
        super.init()
        server.requetsData()
        server.delegate = self
        
    }
    
    func getImage(link: String) -> UIImage {
        return server.getImageFromUrl(link: link)
    }
    
    func getSearchingIndexes (searchText: String) {
        self.indexesOfElementsFoundArray.removeAll()
        
        for i in 0 ..< self.dataOfImageArray.count {
            if let title = self.dataOfImageArray[i].titleOfImage,
               title.contains(searchText) {
                self.indexesOfElementsFoundArray.append(i)
            }
        }
        if !searchText.isEmpty && indexesOfElementsFoundArray.isEmpty {
            self.notResultOfSearch = false
        } else {
            self.notResultOfSearch = true
        }
    }
}

extension ProjectManager: MFMailComposeViewControllerDelegate {
    
    func postEmail(_ image: UIImage) -> MFMailComposeViewController? {
        
        let mail = MFMailComposeViewController()
        let image = image
        let imageData = image.jpegData(compressionQuality: 1)
        let emailBody = "<html><body><p>This is your message</p></body></html>"

        guard MFMailComposeViewController.canSendMail() else {
            print("Mail services are not available")
            return mail
        }
        
        mail.mailComposeDelegate = self
        mail.setSubject("your subject here")
        mail.addAttachmentData(imageData!, mimeType:"image/jpeg", fileName:"Your Filename")
        mail.setMessageBody(emailBody, isHTML:true)
        
        return mail
    }
    
    private func mailComposeController(controller: MFMailComposeViewController,
                               didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismiss(animated: true, completion: nil)
    }
}

extension ProjectManager: ServerDataProtocol {
    
    func setData(_ arrayOfDataImages: [DataOfImage]) {
        self.dataOfImageArray = arrayOfDataImages
        
        delegate?.updateData()
    }
}

protocol ProjectDataProtocol: AnyObject {
    func updateData()
}


