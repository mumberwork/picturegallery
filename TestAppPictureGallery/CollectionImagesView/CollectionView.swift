//
//  CollectionView.swift
//  TestAppPictureGallery
//
//  Created by Артур on 29.08.2021.
//

import UIKit

class CollectionView: UICollectionView {
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.setupProperties()
    }

    private func setupProperties() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        
        layout.sectionInset = UIEdgeInsets(top: 10, left: 15, bottom: 10, right: 15)
        layout.itemSize = CGSize(width: self.frame.width/4, height: self.frame.height/4)
        layout.minimumInteritemSpacing = 10
        layout.minimumLineSpacing = 0
        self.collectionViewLayout = layout
    }
}
