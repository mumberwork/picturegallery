//
//  CollectionImagesViewCellCollectionViewCell.swift
//  TestAppPictureGallery
//
//  Created by Артур on 23.08.2021.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
}
