//
//  DataOfImage.swift
//  TestAppPictureGallery
//
//  Created by Артур on 29.08.2021.
//

import UIKit

struct DataOfImage {
    
    var miniatureImage: UIImage?
    var fullImage: String?
    var titleOfImage: String?
    
    init(miniatureImage: UIImage, fullImage: String, titleOfImage: String) {
        self.miniatureImage = miniatureImage
        self.fullImage = fullImage
        self.titleOfImage = titleOfImage
    }
}
