//
//  Extensions.swift
//  TestAppPictureGallery
//
//  Created by Артур on 23.08.2021.
//

import UIKit
import Foundation

extension String {
  mutating func insert(string:String,ind:Int) {
    self.insert(contentsOf: string, at: self.index(self.startIndex, offsetBy: ind))
  }
}

extension UIView {
    func loadingIndicator(_ value: Bool, hiddenView: Bool = true, color: UIColor = UIColor.white) {
        if value {
            self.alpha = hiddenView ? 0 : 1
            let indicator = UIActivityIndicatorView()
            indicator.style = .medium
            indicator.hidesWhenStopped = true
            indicator.color = color
            
            indicator.startAnimating()
            indicator.translatesAutoresizingMaskIntoConstraints = false
            
            self.superview?.addSubview(indicator)
            NSLayoutConstraint.activate([
                indicator.centerYAnchor.constraint(equalTo: self.centerYAnchor),
                indicator.centerXAnchor.constraint(equalTo: self.centerXAnchor)
            ])
        } else {
            self.superview?.subviews.forEach { (view) in
                if view is UIActivityIndicatorView {
                    if let view = view as? UIActivityIndicatorView {
                        view.stopAnimating()
                    }
                }
            }
            self.alpha = 1
        }
    }
}

